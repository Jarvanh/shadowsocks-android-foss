### 5.0.1 - 2020-01-20
- Add subscription support
- New icon and theme
- Remove built-in ACL rules except Bypass LAN

### 4.8.7a - 2019-12-25
- Rename to Shadowsocks FOSS
- Remove proprietary dependencies (GMS, Fabric and Firebase Crashlytics)
- Update Gradle, Android Gradle plugin and dependencies
- Change default config to route=bypass-lan, remoteDns=1dot1dot1dot1.cloudflare-dns.com, udpdns=true, ipv6=true
- Delete zh-rCN (Simplified Chinese) translation
- Change minSdkVersion from 21 to 23
- Change gradle config from APK Splits to Product Flavors, add version name suffix
- change mipmap format from WebP to PNG
