## [Shadowsocks](https://shadowsocks.org) FOSS for Android

[![API](https://img.shields.io/badge/API-23%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=23)
[![License: GPL-3.0](https://img.shields.io/badge/license-GPL--3.0-orange.svg)](https://www.gnu.org/licenses/gpl-3.0)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/com.gitlab.mahc9kez.shadowsocks.foss/)

This project is a fork of [shadowsocks-andorid](https://github.com/shadowsocks/shadowsocks-android). It aimed to be FOSS, ad-free, does not require any proprietary dependencies or services, and does not collect your data nor share with any third party services.

Currently keep updating from upstream, no features added but only tweak some default configurations and remove zh-rCN translation (NOTE this project does not accept zh-rCN translation). The built-in QR scanner also have been removed because it requires GMS.

### PREREQUISITES

* JDK 1.8
* Android SDK
  - Android NDK

### BUILD

* Clone the repo using `git clone --recurse-submodules <repo>` or update submodules using `git submodule update --init --recursive`
* `sed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' core/src/main/jni/badvpn/tun2socks/tun2socks.c`
* Build it using Android Studio or gradle script

### BUILD WITH DOCKER

```bash
mkdir build
sudo chown 3434:3434 build
docker run --rm -v ${PWD}/build:/build circleci/android:api-28-ndk bash -c "cd /build; git clone https://github.com/shadowsocks/shadowsocks-android; cd shadowsocks-android; git submodule update --init --recursive; ./gradlew assembleDebug"
```

### BUILD WITH GitLab CI

You need to encode related values to Base64 and set the following Environment Variables, check `.gitlab-ci.yml`

```
CI_ANDROID_KEYSTORE
CI_ANDROID_STOREPASS
CI_PRIVATE_TOKEN
```

### Versioning

Version Name:
Same as upstream releases but [a-z] suffix for each revision, nightly releases have `-nightly` suffix

Version Code:

```
 [4]  [08]  [07]  [01]  0
 4     .8    .7     a   [ABI]

ABI:
0 universal
1 armeabi-v7a
2 arm64-v8a
3 x86
4 x86_64
```


## OPEN SOURCE LICENSES

<ul>
    <li>redsocks: <a href="https://github.com/shadowsocks/redsocks/blob/shadowsocks-android/README">APL 2.0</a></li>
    <li>mbed TLS: <a href="https://github.com/ARMmbed/mbedtls/blob/development/LICENSE">APL 2.0</a></li>
    <li>libevent: <a href="https://github.com/shadowsocks/libevent/blob/master/LICENSE">BSD</a></li>
    <li>tun2socks: <a href="https://github.com/shadowsocks/badvpn/blob/shadowsocks-android/COPYING">BSD</a></li>
    <li>pcre: <a href="https://android.googlesource.com/platform/external/pcre/+/master/dist2/LICENCE">BSD</a></li>
    <li>libancillary: <a href="https://github.com/shadowsocks/libancillary/blob/shadowsocks-android/COPYING">BSD</a></li>
    <li>shadowsocks-libev: <a href="https://github.com/shadowsocks/shadowsocks-libev/blob/master/LICENSE">GPLv3</a></li>
    <li>libev: <a href="https://github.com/shadowsocks/libev/blob/master/LICENSE">GPLv2</a></li>
    <li>libsodium: <a href="https://github.com/jedisct1/libsodium/blob/master/LICENSE">ISC</a></li>
</ul>


### LICENSE

The files under [art/] folder which are used to generate certain icons are licensed under Apache-2.0

Based heavily on [shadowsocks-andorid](https://github.com/shadowsocks/shadowsocks-android) by Max Lv and Mygod Studio

Copyright (C) 2017 by Max Lv <<max.c.lv@gmail.com>>  
Copyright (C) 2017 by Mygod Studio <<contact-shadowsocks-android@mygod.be>>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
