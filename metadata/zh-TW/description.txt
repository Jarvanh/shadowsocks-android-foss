Shadowsocks FOSS 是一個無廣告，自由且開放原始碼的 Shadowsocks 用戶端，它不依賴任何專有依賴或服務。

Shadowsocks 是一個高效能跨平台的安全的 SOCKS5 代理，它幫助你私密且安全地瀏覽網路。

### FEATURES ###

1. Bleeding edge techniques with Asynchronous I/O and Event-driven programming.
2. Low resource comsumption, suitable for low end boxes and embedded devices.
3. Avaliable on multiple platforms, including PC, MAC, Mobile (Android and iOS) and Routers (OpenWRT).
4. Open source implementions in python, node.js, golang, C#, and pure C.

More details about Shadowsocks: https://shadowsocks.org

### 設定 ###

當前無內建 QR Code 掃碼器，但可以使用其他應用程式掃描 QR Code ，拷貝至剪貼簿後匯入。也可從檔案匯入或手動設置。
此應用程式不蒐集或與任何第三方服務共享資料，但你所使用的節點所屬之網路代理服務商可能會蒐集你的資料。
出於隱私需要你可能需要自建 Shadowsocks 伺服端，詳見 https://shadowsocks.org/en/download/servers.html

### 外掛程式 ###

V2Ray Plugin
https://f-droid.org/en/packages/com.github.shadowsocks.plugin.v2ray/
