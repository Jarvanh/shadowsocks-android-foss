#!/bin/bash


## Patch 1: Fixes

function res-fix-banner {
	# move banner from mipmap to drawable
	mkdir core/src/main/res/drawable-{mdpi,hdpi,xhdpi,xxhdpi,xxxhdpi}
	mv core/src/main/res/{mipmap,drawable}-anydpi-v24/banner.xml
	mv core/src/main/res/{mipmap,drawable}-mdpi/banner.webp
	mv core/src/main/res/{mipmap,drawable}-hdpi/banner.webp
	mv core/src/main/res/{mipmap,drawable}-xhdpi/banner.webp
	mv core/src/main/res/{mipmap,drawable}-xxhdpi/banner.webp
	mv core/src/main/res/{mipmap,drawable}-xxxhdpi/banner.webp
	rmdir core/src/main/res/mipmap-anydpi-v24
}

## Patch 2 FOSS

function gradle-clean-dep {
	rm {mobile,tv}/google-services.json

	# build.gradle
	gsed -i '/ben-manes/d' build.gradle
	gsed -i '/maven {/,+2d' build.gradle
	gsed -i '/maven-publish/d' build.gradle
	# core/build.gradle
	#gsed -i '/abiFilters/d' core/build.gradle
	# {mobile,tv}/build.gradle
	#gsed -i '/splits {/,+5d; /ext.abiCodes/,+6d' {mobile,tv}/build.gradle # [double-check]
	# plugin/build.gradle
	gsed -i '/com.vanniktech.maven.publish/d' plugin/build.gradle
	gsed -i '/testInstrumentationRunner/d' plugin/build.gradle
	gsed -i '/mavenPublish {/,+11d' plugin/build.gradle # [double-check]
	# common
	gsed -i '/gms/d; /fabric/d; /firebase/d; /crashlytics/d; /work-gcm/d; /barcodescanner/d' {,core/,mobile/,plugin/,tv/}build.gradle
	# minSdkVersion
	gsed -i 's/minSdkVersion = 21/minSdkVersion = 23/' build.gradle
	# Proguard rules
	#gsed -i '/com.google.android.gms/d' {mobile,tv}/proguard-rules.pro # no such file
}

function src-clean {
	# remove
	rm core/src/main/java/com/github/shadowsocks/bg/RemoteConfig.kt mobile/src/main/java/com/github/shadowsocks/ScannerActivity.kt
	# edit
	#gsed -i '/crashlytics/Id; /firebase/Id; /fabric/Id; /Core.analytics.logEvent/d; /com.google.android.gms/d' $(grep -ril --exclude-dir=.git --exclude=\*.{md,acl,xml} 'fabric\|gms\|firebase\|crashlytics' .)
	gsed -i '/crashlytics/Id; /firebase/Id; /fabric/Id; /Core.analytics.logEvent/d; /com.google.android.gms/d' $(grep -ril --include=\*.kt 'fabric\|gms\|firebase\|crashlytics' {core,mobile,plugin,tv}/src/main/java)
	gsed -i '/last exit code:/d' core/src/main/java/com/github/shadowsocks/bg/GuardedProcessPool.kt
	gsed -i '/This will probably not work as intended/d' core/src/main/java/com/github/shadowsocks/acl/Acl.kt

	#gsed -i '/com.google.android.gms.ads/d' mobile/src/main/java/com/github/shadowsocks/ProfilesFragment.kt # [manually] remove Ads
	gsed -i '/R.id.action_scan_qr_code/,+3d; /nativeAdView == null/,+21d; /fun populateUnifiedNativeAdView/,+55d; /NativeAd/Id' mobile/src/main/java/com/github/shadowsocks/ProfilesFragment.kt
	#gsed -i '/RemoteConfig.scheduleFetch/d; /profile.isSponsored/,+30d' core/src/main/java/com/github/shadowsocks/bg/ProxyInstance.kt # [double-check]
	gsed -i '/RemoteConfig.fetchAsync/d; /profile.isSponsored/,+30d' core/src/main/java/com/github/shadowsocks/bg/ProxyInstance.kt # [double-check]

	gsed -i '/osslicense/Id' $(grep -ril --include=\*.kt osslicense {core,mobile,plugin,tv}/src/main/java)
	gsed -i '/Key.aboutOss/,+2d' tv/src/main/java/com/github/shadowsocks/tv/MainPreferenceFragment.kt
}

function manifest-clean {
	# AndroidManifest.xml (core) [double-check]
	gsed -i 's#@mipmap/banner#@drawable/banner#' core/src/main/AndroidManifest.xml
	gsed -i '/com.google.android.backup.api_key/,+1d' core/src/main/AndroidManifest.xml
	gsed -i '/com.google.firebase/,+1d; /com.crashlytics.android/,+1d' core/src/main/AndroidManifest.xml
	gsed -i '/Used for API < 23/,+5d' core/src/main/AndroidManifest.xml
	# AndroidManifest.xml (mobile) [manually]  remove .ScannerActivity and meta-data
	gsed -i '/android.permission.CAMERA/,+1d; /android.hardware.camera/,+1d' mobile/src/main/AndroidManifest.xml
	echo "$(tac mobile/src/main/AndroidManifest.xml | gsed '/ca-app-pub-3283768469187309~3571758745/,+2d' | tac)" >mobile/src/main/AndroidManifest.xml
	echo "$(gsed '/add_profile_methods_scan_qr_code/,+2d' mobile/src/main/AndroidManifest.xml | tac | gsed '/ScannerActivity/,+1d' | tac)" >mobile/src/main/AndroidManifest.xml
}

function res-clean {
	# remove
	rm mobile/src/main/res/layout/{ad_unified.xml,layout_scanner.xml}
	# edit
	gsed -i '/action_scan_qr_code/,+2d' mobile/src/main/res/menu/profile_manager_menu.xml # [double-check]
	gsed -i '/android:shortcutId="scan"/,+7d' mobile/src/main/res/xml/shortcuts.xml

	echo "$(cat tv/src/main/res/xml/pref_main.xml | tac | gsed '/preferences_license_summary/,+3d' | tac)" >tv/src/main/res/xml/pref_main.xml
}


## PAtch 3 Translation & Rebrand

BRANCH=base
function remove-zh-cn {
	rm -rf {core,plugin}/src/main/res/values-zh-rCN/
	gsed -i "s/'zh-rCN', //" build.gradle
	gsed -i '/Chinese.*simplified/d' translate.py
	gsed -i '/zh-CN/d' translate.py
}

function remove-some-route {
	gsed -i '/route_entry_bypass_chn/d; /route_entry_bypass_lan_chn/d; /route_entry_gfwlist/d; /route_entry_chinalist/d' core/src/main/res/values*/strings.xml mobile/src/main/res/values/arrays.xml
	gsed -i '/bypass-china/d; /bypass-lan-china/d; /gfwlist/d; /china-list/d' mobile/src/main/res/values/arrays.xml
	rm core/src/main/assets/acl/{bypass-china.acl,bypass-lan-china.acl,china-list.acl,gfwlist.acl}

	gsed -i 's/Acl.BYPASS_CHN, //' core/src/main/java/com/github/shadowsocks/bg/VpnService.kt
	gsed -i '/action_import_gfwlist/,+1d' mobile/src/main/res/menu/custom_rules_menu.xml
	gsed -i '/R.id.action_import_gfwlist/,+6d' mobile/src/main/java/com/github/shadowsocks/acl/CustomRulesFragment.kt

	# Optional
	gsed -i '/BYPASS_CHN/d; /BYPASS_LAN_CHN/d; /GFWLIST/d; /CHINALIST/d' core/src/main/java/com/github/shadowsocks/acl/Acl.kt
	echo "$(tac core/src/main/java/com/github/shadowsocks/net/HttpsTest.kt | gsed '/generate_204/,+2d' | tac | gsed 's#val url =.*$#val url = URL("https", "www.google.com", "/generate_204")#')" >core/src/main/java/com/github/shadowsocks/net/HttpsTest.kt
}

# Trust FDroid (V2ray Plugin)
# core/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/shadowsocks/plugin/PluginManager.kt

function change-profile-config {
	gsed -i '/var host: String = sponsored,/s/sponsored/"127.0.0.1"/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/var password: String = "u1rRWTssNv0p",/s/u1rRWTssNv0p/password/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/var method: String = "aes-256-cfb",/s/aes-256-cfb/chacha20-ietf/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/var route: String = "all",/s/all/bypass-lan/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/var remoteDns: String = "dns.google",/s/dns.google/1dot1dot1dot1.cloudflare-dns.com/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/var udpdns: Boolean = false,/s/false/true/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/var ipv6: Boolean = false,/s/false/true/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
	gsed -i '/@TargetApi(28)/s/28/29/' core/src/main/java/com/github/shadowsocks/database/Profile.kt
}

function rebrand {
	# Package Name
	gsed -i 's/com.github.shadowsocks.tv/com.gitlab.mahc9kez.shadowsocks.tv.foss/; s/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' {mobile,tv}/build.gradle
	gsed -i 's/targetPackage="com.github.shadowsocks"/targetPackage="com.gitlab.mahc9kez.shadowsocks.foss"/' mobile/src/main/res/xml/{pref_profile.xml,shortcuts.xml}
	# Name
	gsed -i 's/"shadowsocks"/"Shadowsocks FOSS"/' mobile/src/main/res/layout/navigation_header.xml
	gsed -i 's/Shadowsocks/Shadowsocks FOSS/' core/src/main/res/values*/strings.xml
	# Font
	gsed -i 's#@font/iceland#@font/aquifer#' mobile/src/main/res/layout/navigation_header.xml
	gsed -i 's/"32sp"/"24sp"/' mobile/src/main/res/layout/navigation_header.xml

	## Icon remove ic_launcher and ic_service
	rm -rf core/src/main/res/mipmap* core/ic_launcher-web.png
	rm core/src/main/res/drawable-anydpi-v24/ic_launcher_foreground.xml
	rm core/src/main/res/color-v24/ic_launcher_foreground_shadow.xml && rmdir core/src/main/res/color-v24
	rm core/src/main/res/values/colors.xml
	rm core/src/main/res/drawable/ic_service_active.xml
	## Icon add from another branch
	git checkout $BRANCH -- mobile/src/main/res/drawable/background_header.webp
	# ic_launcher
	git checkout $BRANCH -- core/src/main/ic_launcher-web.png
	git checkout $BRANCH -- core/src/main/res/mipmap-{m,h,xh,xxh,xxxh}dpi/{ic_launcher.png,ic_launcher_round.png}
	git checkout $BRANCH -- core/src/main/res/mipmap-anydpi-v26/{ic_launcher.xml,ic_launcher_round.xml}
	git checkout $BRANCH -- core/src/main/res/drawable-v24/ic_launcher_foreground.xml
	git checkout $BRANCH -- core/src/main/res/values/ic_launcher_background.xml
	# ic_service_active
	git checkout $BRANCH -- core/src/main/res/drawable-{m,h,xh,xxh}dpi/ic_service_active.png
	git checkout $BRANCH -- core/src/main/res/drawable-anydpi-v24/ic_service_active.xml
	# theme color
	git checkout $BRANCH -- plugin/src/main/res/values/colors.xml
	git checkout $BRANCH -- mobile/src/main/res/drawable/ic_qu_shadowsocks_foreground.xml

	# Font, about.html and privacy_policy.md
	rm mobile/src/main/res/font/iceland.ttf
	git checkout $BRANCH -- mobile/src/main/res/font/aquifer.ttf
	git checkout $BRANCH -- mobile/src/main/res/raw/about.html
	rm privacy_policy.md

	# FAQ url
	gsed -i 's#https://github.com/shadowsocks/shadowsocks-android/blob/master/.github/faq.md#https://gitlab.com/mahc9kez/shadowsocks-android-foss/blob/master/.github/faq.md#' core/src/main/res/values/strings.xml
}

function rename-pkg {
	gsed -i 's/com.github.shadowsocks.QRCodeDialog.KEY_URL/com.gitlab.mahc9kez.shadowsocks.foss.QRCodeDialog.KEY_URL/' mobile/src/main/java/com/github/shadowsocks/ProfilesFragment.kt
	# src
	grep -ril --include=\*.{aidl,java,kt,xml} com.github.shadowsocks {core,mobile,tv}/src | xargs gsed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/'
	grep -ril --include=\*.{aidl,java,kt,xml} com.gitlab.mahc9kez.shadowsocks.foss.plugin {core,mobile,tv}/src | xargs gsed -i 's/com.gitlab.mahc9kez.shadowsocks.foss.plugin/com.github.shadowsocks.plugin/'

	mkdir -p core/src/main/aidl/com/gitlab/mahc9kez/shadowsocks/foss
	mv core/src/main/aidl/com/github/shadowsocks/aidl core/src/main/aidl/com/gitlab/mahc9kez/shadowsocks/foss/aidl
	mkdir -p core/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss
	mv core/src/main/java/com/github/shadowsocks core/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss

	mkdir -p mobile/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss
	mv  mobile/src/main/java/com/github/shadowsocks mobile/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss

	#mkdir -p plugin/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/plugin
	#mv plugin/src/main/java/com/github/shadowsocks/plugin plugin/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/plugin

	mkdir -p tv/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/tv
	mv tv/src/main/java/com/github/shadowsocks/tv tv/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/tv

	mv core/schemas/com.github.shadowsocks.database.PrivateDatabase core/schemas/com.gitlab.mahc9kez.shadowsocks.foss.database.PrivateDatabase
	mv core/schemas/com.github.shadowsocks.database.PublicDatabase core/schemas/com.gitlab.mahc9kez.shadowsocks.foss.database.PublicDatabase

	gsed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' core/src/main/jni/include/pdnsd/config.h
	#gsed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' core/src/main/jni/badvpn/tun2socks/tun2socks.c
	gsed -i 's/com_github_shadowsocks/com_gitlab_mahc9kez_shadowsocks_foss/' core/src/main/jni/jni-helper.cpp
	gsed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' plugin/gradle.properties

	rmdir core/src/main/aidl/com/github/{shadowsocks,} core/src/main/java/com/github tv/src/main/java/com/github/{shadowsocks,} mobile/src/main/java/com/github

}

function manually-add {
	#git checkout $BRANCH -- mobile/src/main/AndroidManifest.xml
	#git checkout $BRANCH -- mobile/src/main/java/com/github/shadowsocks/ProfilesFragment.kt
	#git checkout $BRANCH -- mobile/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/shadowsocks/ProfilesFragment.kt
	#git checkout $BRANCH -- {core,mobile,tv}/build.gradle
	git checkout $BRANCH -- release.sh
	git checkout $BRANCH -- CHANGELOG.md
	git checkout $BRANCH -- CONTRIBUTING.md
	git checkout $BRANCH -- README.md

	git checkout $BRANCH -- metadata
	git checkout $BRANCH -- art
	git checkout $BRANCH -- patch.sh
	git checkout $BRANCH -- .gitlab-ci.yml
}

function bump-version {
	# version v5.0.5
	gsed -i 's/versionCode = 5000550/versionCode = 50005000/' build.gradle
	gsed -i '/versionName = /s/-nightly//' build.gradle
	gsed -i 's/versionCode rootProject.versionCode/versionCode 50005000/' mobile/build.gradle
	gsed -i 's/versionName rootProject.versionName/versionName "5.0.5"/' mobile/build.gradle
}

## Edit {core,mobile,tv}/build.gradle to include nightly and abi configuration
# gsed -i '/abiFilters/d' core/build.gradle
# gsed -i '/splits {/,+5d; /ext.abiCodes/,+6d' {mobile,tv}/build.gradle

# Trust FDroid (V2ray Plugin)
# core/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/shadowsocks/plugin/PluginManager.kt


function patch-1 {
	res-fix-banner
}
function patch-2 {
	gradle-clean-dep
	src-clean
	manifest-clean
	res-clean
}
function patch-3 {
	remove-zh-cn
	remove-some-route
	change-profile-config
	rebrand
	rename-pkg
	manually-add
	bump-version
}
function patch-4 {
	gradle wrapper --gradle-version 6.1.1 --distribution-type all --gradle-distribution-sha256-sum 10065868c78f1207afb3a92176f99a37d753a513dff453abb6b5cceda4058cda
	gsed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' core/src/main/jni/badvpn/tun2socks/tun2socks.c
	git checkout $BRANCH -- {core,mobile,tv}/build.gradle
	git checkout $BRANCH -- core/src/main/java/com/gitlab/mahc9kez/shadowsocks/foss/shadowsocks/plugin/PluginManager.kt
}

patch-1
patch-2
patch-3
patch-4

